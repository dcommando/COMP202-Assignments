/*
Name: David Billing
id: 1211447
username: dhb5
*/

import java.net.*;
import java.io.*;
import java.util.*;

//http://localhost.com:8080/HttpServer.java

class HttpServer {
    public static void main(String args[]) {
		try {
			//get the port args
			int port = Integer.parseInt(args[0]);
			
			//String file = args[1];
			
			//new socket to accept incoming transmissions
			ServerSocket server = new ServerSocket(port);

			while(true) {
				Socket client = server.accept();
				
				//The thread that will handle the server
				HttpServerSession session = new HttpServerSession(client);
				session.start();
				
				//get the address and name of the connecting client
				InetAddress IP = client.getInetAddress();
				String address = IP.getHostName();
				String addIP = IP.getHostAddress();
							
				//to ensure everything is working
				System.out.println("Hello, " + address);
				System.out.println("Your IP address is :" + addIP);
				
				//Everyone likes a tidy dinosaur!
				//client.close();
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
			System.out.println("Please provide a sensible socket number and file that is in the directory.");
		}
    }
}

//handles the Http Request
class HttpServerSession extends Thread {
    private Socket client;
    String request;
	String file;
	int endOfFile = 1;
	int counter = 0;
    FileInputStream fileStream;

    //Execute operations
    public void run() {
		try {
			//enable reader so that error message can be sent if invalid file supplied
			BufferedOutputStream bos = new BufferedOutputStream(client.getOutputStream());
			
			try {
				//for reading files into
				byte[] byteArr = new byte[1024];

				//for reading the client and file
				BufferedReader r = new BufferedReader( new InputStreamReader(client.getInputStream()));

				//ready to proceed?
				r.ready();

				//read the line
				request = r.readLine();
				
				//Display the requested header
				//System.out.println(request);

				//split the header line
				String split[] = request.split(" ");
				
				//if the header is malformed...
				if (split.length < 3) throw new Exception();

				//get the filename out of the argument list
				file = split[1].substring(1);
				
				//if we do not have this file, return index.
				if (file.compareTo("") == 0) {
				    file = "index.html";
				}
				
				//Display what is being looked for
				System.out.println("The file we are looking for is: " + file);
				
				//Open the file to be displayed
				fileStream = new FileInputStream(file);
			   
				//read the data from the browser
				while(true) {
				String line = r.readLine();

					if(line == null) {
						System.out.println("oops, too far");
						throw new Exception();
					}
					if(line.compareTo("") == 0) break;
				}
				
				//send header
				println(bos, "HTTP/1.1 200 OK");
				println(bos, "");
				
				//hold the bytes read into the array
				int rc = 0;

				//while not at the end of file
				while ((rc = fileStream.read(byteArr)) != -1) {
					sleep(500);
				    bos.write(byteArr, 0, rc);
					// counter++;
					// System.out.println(counter);
					bos.flush();
				}
				//release any resources
				bos.close();
				r.close();
				fileStream.close();
				client.close();
				
			//if we have not found the file, send a 404 message
			} catch (FileNotFoundException e) {
				println(bos, "HTTP/1.1 404 FILE_NOT_FOUND");
				println(bos, "");
				println(bos, "Could not find the file you were looking for, ERROR: 404");
				println(bos, "Sorry, " + file + " does not exist");
				client.close();
			//generic catch
			} catch (Exception e) {
				System.out.println("Error: " + e);
			}
		//if opening the client did not work.
		} catch (Exception e) {
			System.out.println("Could not get the client output stream.");
			System.out.println("Error code: " + e);
		}
    }

	//method to print string to the client
    public void println(BufferedOutputStream bos, String s) throws IOException {
		//add necessary linefeed and carriage return on
		String out = s + "\r\n";
		byte[] array = out.getBytes();
		for (int i = 0; i<array.length; i++) {
			bos.write(array[i]);
		}
		//send the data away;
		bos.flush();
		return;
    }
    
    //constructor takes only a socket (now defunct)
    public HttpServerSession(Socket socket) {
		this.client = socket;
    }
	
	//socket and file to open
	public HttpServerSession(Socket socket, String file) {
		this.client = socket;
		this.file = file;
	}
}
/*............................................................END.OF.LINE............................................................*/
